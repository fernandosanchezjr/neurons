package neurons

type External struct {
	*Connected
}

func NewExternal(id int, inputs int) *External {
	e := &External{}
	e.Connected = NewConnected(id, inputs, e.handleClientMessage)
	return e
}

func (e *External) handleClientMessage(id int, message Message) {
	 println("External id", e.Id, "received message", message.String(), "from input", id)
}