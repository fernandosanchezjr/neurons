package neurons

type Output struct {
	*Connected
}

func NewOutput(id int, inputs int) *Output {
	e := &Output{}
	e.Connected = NewConnected(id, inputs, e.handleClientMessage)
	return e
}

func (e *Output) handleClientMessage(id int, message Message) {
	println(e.Id, "received message", message, "from", id)
}