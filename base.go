package neurons

import (
	"errors"
	"github.com/goml/gobrain"
)

type Base struct {
	TestIterations int
	Inputs         int
	LRate          float64
	MFactor        float64
	Brain          *gobrain.FeedForward
	TrainingData   [][][]float64
}

func NewBase(inputs int) *Base {
	ret := &Base{TestIterations: 100000, LRate: 0.6, MFactor: 0.4, Brain: &gobrain.FeedForward{},
		Inputs: inputs}
	ret.ClearTrainingData()
	ret.Brain.Init(inputs, inputs*3, 1)
	return ret
}

func (wn *Base) AddTrainingEntry(result float64, inputs ...float64) {
	if len(inputs) != wn.Inputs {
		panic(errors.New("invalid input count for update"))
	}
	wn.TrainingData = append(wn.TrainingData, [][]float64{inputs, {result}})
}

func (wn *Base) Train(iterations int, lRate float64, mFactor float64, debug bool) []float64 {
	return wn.Brain.Train(wn.TrainingData, iterations, lRate, mFactor, debug)
}

func (wn *Base) ClearTrainingData() {
	wn.TrainingData = make([][][]float64, 0)
}

func (wn *Base) PerformTraining() {
	if len(wn.TrainingData) > 0 {
		wn.Train(wn.TestIterations, wn.LRate, wn.MFactor, true)
		wn.Brain.Test(wn.TrainingData)
		wn.ClearTrainingData()
	}
}

func (wn *Base) Update(inputs ...float64) float64 {
	if len(inputs) != wn.Inputs {
		panic(errors.New("invalid input count for update"))
	}
	result := wn.Brain.Update(inputs)
	return result[0]
}
