package neurons

type Hidden struct {
	*Connected
}

func NewHidden(id int, inputs int) *Hidden {
	e := &Hidden{}
	e.Connected = NewConnected(id, inputs, e.handleClientMessage)
	return e
}

func (e *Hidden) handleClientMessage(id int, message Message) {
	println(e.Id, "received message", message, "from", id)
}