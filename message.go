package neurons

import "fmt"

type Message interface {
	IsMessage()
	String() string
}

type BaseMessage struct {
	Frame int64
	Value float64
	Id    int64
}

func (b BaseMessage) IsMessage() {

}

func (b BaseMessage) String() string {
	return fmt.Sprintf("Id: %d, Frame: %d, Value: %f", b.Id, b.Frame, b.Value)
}