package neurons

import (
	"bitbucket.org/fernandosanchezjr/bus"
	"sort"
	"fmt"
)

type Connection struct {
	Bus    int
	Client int
}

type Network struct {
	External []*External
	Hidden []*Hidden
	Output []*Output
	ExternalMap map[int][]int
	HiddenMap 		map[int][]int
	OutputMap 		map[int][]int
	ConnectivityMap []*Connection
	network *bus.Network
}

func NewNetwork() *Network {
	n := &Network{External: make([]*External, 0), network: bus.NewNetwork(),
		ExternalMap: make(map[int][]int), HiddenMap: make(map[int][]int),
		OutputMap:make(map[int][]int), ConnectivityMap: make([]*Connection, 0)}
	return n
}

func (n *Network) Count() int {
	return len(n.External) + len(n.Hidden) + len(n.Output)
}

func (n *Network) AddExternal(inputs int) {
	e := NewExternal(n.Count() + 1, inputs)
	n.External = append(n.External, e)
	n.network.SetBus(e.Id, e.Bus())
	inputIds := make([]int, 0, inputs)
	for _, c := range e.Clients() {
		id := n.network.ClientCount() + 1
		n.network.SetClient(id, c)
		inputIds = append(inputIds, id)
	}
	n.ExternalMap[e.Id] = inputIds
}

func (n *Network) GetExternalIds() []int {
	keys := make([]int, 0, len(n.ExternalMap))
	for k := range n.ExternalMap {
		keys = append(keys, k)
	}
	sort.Ints(keys)
	return keys
}

func (n *Network) GetExternalBuses() ([]*bus.Bus, error) {
	ids := n.GetExternalIds()
	buses := make([]*bus.Bus, 0, len(ids))
	for _, id := range ids {
		if b, err := n.network.GetBus(id); err != nil {
			return nil, err
		} else {
			buses = append(buses, b)
		}
	}
	return buses, nil
}

func (n *Network) GetExternalClientIds(external int) ([]int, error) {
	if ids, ok := n.ExternalMap[external]; !ok {
		return nil, fmt.Errorf("external id %d not found", external)
	} else {
		return ids, nil
	}
}

func (n *Network) GetExternalClients(external int) ([]*bus.Client, error) {
	if ids, err := n.GetExternalClientIds(external); err != nil {
		return nil, err
	} else {
		clients := make([]*bus.Client, 0, len(ids))
		for _, id := range ids {
			if c, err := n.network.GetClient(id); err != nil {
				return nil, err
			} else {
				clients = append(clients, c)
			}
		}
		return clients, nil
	}
}

func (n *Network) GetAllExternalClients() ([]*bus.Client, error) {
	ids := n.GetExternalIds()
	clients := make([]*bus.Client, 0)
	for _, id := range ids {
		if cs, err := n.GetExternalClients(id); err != nil {
			return nil, err
		} else {
			clients = append(clients, cs...)
		}
	}
	return clients, nil
}

func (n *Network) AddHidden(inputs int) {
	e := NewHidden(n.Count() + 1, inputs)
	n.Hidden = append(n.Hidden, e)
	n.network.SetBus(e.Id, e.Bus())
	inputIds := make([]int, 0, inputs)
	for _, c := range e.Clients() {
		id := n.network.ClientCount() + 1
		n.network.SetClient(id, c)
		inputIds = append(inputIds, id)
	}
	n.HiddenMap[e.Id] = inputIds
}

func (n *Network) GetHiddenIds() []int {
	keys := make([]int, 0, len(n.HiddenMap))
	for k := range n.HiddenMap {
		keys = append(keys, k)
	}
	sort.Ints(keys)
	return keys
}

func (n *Network) GetHiddenBuses() ([]*bus.Bus, error) {
	ids := n.GetHiddenIds()
	buses := make([]*bus.Bus, 0, len(ids))
	for _, id := range ids {
		if b, err := n.network.GetBus(id); err != nil {
			return nil, err
		} else {
			buses = append(buses, b)
		}
	}
	return buses, nil
}

func (n *Network) GetHiddenClientIds(hidden int) ([]int, error) {
	if ids, ok := n.HiddenMap[hidden]; !ok {
		return nil, fmt.Errorf("hidden id %d not found", hidden)
	} else {
		return ids, nil
	}
}

func (n *Network) GetHiddenClients(hidden int) ([]*bus.Client, error) {
	if ids, err := n.GetHiddenClientIds(hidden); err != nil {
		return nil, err
	} else {
		clients := make([]*bus.Client, 0, len(ids))
		for _, id := range ids {
			if c, err := n.network.GetClient(id); err != nil {
				return nil, err
			} else {
				clients = append(clients, c)
			}
		}
		return clients, nil
	}
}

func (n *Network) AddOutput(inputs int) {
	e := NewOutput(n.Count() + 1, inputs)
	n.Output = append(n.Output, e)
	n.network.SetBus(e.Id, e.Bus())
	inputIds := make([]int, 0, inputs)
	for _, c := range e.Clients() {
		id := n.network.ClientCount() + 1
		n.network.SetClient(id, c)
		inputIds = append(inputIds, id)
	}
	n.OutputMap[e.Id] = inputIds
}

func (n *Network) GetOutputIds() []int {
	keys := make([]int, 0, len(n.OutputMap))
	for k := range n.OutputMap {
		keys = append(keys, k)
	}
	sort.Ints(keys)
	return keys
}

func (n *Network) GetOutputBuses() ([]*bus.Bus, error) {
	ids := n.GetOutputIds()
	buses := make([]*bus.Bus, 0, len(ids))
	for _, id := range ids {
		if b, err := n.network.GetBus(id); err != nil {
			return nil, err
		} else {
			buses = append(buses, b)
		}
	}
	return buses, nil
}

func (n *Network) GetOutputClientIds(output int) ([]int, error) {
	if ids, ok := n.OutputMap[output]; !ok {
		return nil, fmt.Errorf("output id %d not found", output)
	} else {
		return ids, nil
	}
}

func (n *Network) GetOutputClients(output int) ([]*bus.Client, error) {
	if ids, err := n.GetOutputClientIds(output); err != nil {
		return nil, err
	} else {
		clients := make([]*bus.Client, 0, len(ids))
		for _, id := range ids {
			if c, err := n.network.GetClient(id); err != nil {
				return nil, err
			} else {
				clients = append(clients, c)
			}
		}
		return clients, nil
	}
}

func (n *Network) Wait() {
	n.network.Wait()
}

func (n *Network) Connect(bus int, client int) error {
	if err := n.network.Connect(bus, client); err != nil {
		return err
	} else {
	 	n.ConnectivityMap = append(n.ConnectivityMap, &Connection{Bus: bus, Client: client})
	 	return nil
	}
}

func (n *Network) Disconnect(bus int, client int) error {
	if err := n.network.Disconnect(bus, client); err != nil {
		return err
	} else {
		connectivityMap := make([]*Connection, 0)
		for _, cm := range n.ConnectivityMap {
			if cm.Bus != bus && cm.Client != client {
				connectivityMap = append(n.ConnectivityMap, &Connection{Bus: bus, Client: client})
			}
		}
		n.ConnectivityMap = connectivityMap
		return nil
	}
}