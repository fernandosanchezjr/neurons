package neurons

import (
	"bitbucket.org/fernandosanchezjr/bus"
	"reflect"
)

type Connected struct {
	Id  int
	*Base
	bus *bus.Bus
	clients []*bus.Client
}

func NewConnected(id int, inputs int, f interface{}) *Connected {
	messageType := reflect.TypeOf((*Message)(nil)).Elem()
	e := &Connected{Id: id, Base: NewBase(inputs),
		bus: bus.NewBus(messageType),
		clients: make([]*bus.Client, 0, inputs)}
	for i := 0; i < inputs; i++ {
		client := bus.NewClient(messageType)
		if err := client.AddHandler(f); err != nil {
			panic(err)
		}
		e.clients = append(e.clients, client)
	}
	return e
}

func (e *Connected) Bus() *bus.Bus {
	return e.bus
}

func (e *Connected) Clients() []*bus.Client {
	return e.clients
}
